#  Copyright (C) 2017 Codethink Limited
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Jonathan Maw <jonathan.maw@codethink.co.uk>

import os
import re
import sys

from . import CONTROL_FILE

def split_fields(section):
    fields = []
    current_line = ""
    for line in section.splitlines():
        if re.match("^[\w-]+:", line):
            if current_line:
                fields.append(current_line)
            current_line = line
        else:
            current_line += line
    fields.append(current_line)
    return fields


def read_section(section):

    section_data = {}
    for field in split_fields(section):
        m = re.match(r'^([\w-]+):\s+(.*)$', field, re.DOTALL)
        if not m:
            sys.exit("Failed to split field '{}' in section '{}'".format(field, section))
        section_data[m.group(1)] = m.group(2)

    return section_data


def strip_comments(text):
    return re.sub("#.*\n", "", text)


def parse_controldata(args, repository):
    controlpath = os.path.join(repository, CONTROL_FILE)
    with open(controlpath, "r") as f:
        controltext = strip_comments(f.read())
    controlsections = controltext.split("\n\n")
    controldata = {"packages": {}, "repository": repository}
    for sec in controlsections:
        if not sec:
            continue

        secdata = read_section(sec)
        if "Source" in secdata:
            controldata["source"] = secdata
        else:
            controldata["packages"][secdata["Package"]] = secdata
    controldata["bstfile"] = "{}{}.bst".format(args["element-prefix"] or "",
                                               controldata["source"]["Source"])

    return controldata
